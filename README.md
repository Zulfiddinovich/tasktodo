
# Todo android app
This project is Todo app for android.

<img src="https://img.shields.io/badge/license-MIT_license-blue"/>


# Features
1. It uses **single activity** which makes strait-forward task managing capability
2. It supports swipe (right to **delete** and left to **edit**)
3. **Tick** a task as it is done or remove
3. Keeps four personal tasks for a **lifelong period**


# Images

<img src="https://gitlab.com/Zulfiddinovich/tasktodo/-/raw/master/files/images/Main_photo_PlayMarket.jpg" style="border:3px outset silver;"/>

<img src="https://gitlab.com/Zulfiddinovich/tasktodo/-/raw/master/files/images/1_.jpg" style="border:3px outset silver;" width="250"/>
<img src="https://gitlab.com/Zulfiddinovich/tasktodo/-/raw/master/files/images/2_.jpg" style="border:3px outset silver;" width="250"/>
<img src="https://gitlab.com/Zulfiddinovich/tasktodo/-/raw/master/files/images/3_.jpg" style="border:3px outset silver;" width="250"/>
<img src="https://gitlab.com/Zulfiddinovich/tasktodo/-/raw/master/files/images/4_.jpg" style="border:3px outset silver;" width="250"/>
<img src="https://gitlab.com/Zulfiddinovich/tasktodo/-/raw/master/files/images/5_.jpg" style="border:3px outset silver;" width="250"/>


# License
Project`s License - [here](https://gitlab.com/Zulfiddinovich/tasktodo/-/raw/master/LICENSE.txt)